package com.example.counterhomework;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int counter = 0;
    private TextView counterTextView;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        counterTextView = findViewById(R.id.counterTextView);
        editText = findViewById(R.id.editText);

        if (savedInstanceState != null) {
            counter = savedInstanceState.getInt("counter");
            counterTextView.setText(String.valueOf(counter));
            editText.setText(savedInstanceState.getString("editText"));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counter", counter);
        outState.putString("editText", editText.getText().toString());
    }

    public void handleIncreaseCounterClick(View view) {
        counter++;
        counterTextView.setText(String.valueOf(counter));
    }
}